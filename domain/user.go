package domain

type User struct {
	ID        string
	UserName  string
	LastName  string
	FirstName string
	Password  string
	Email     string
	Mobile    string
	ASZF      bool
}
