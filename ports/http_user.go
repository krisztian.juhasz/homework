package ports

import (
	"it-test/app/query"
	"it-test/domain"
	"it-test/pkg/server/httperr"
	"net/http"

	"github.com/go-chi/render"
	"github.com/google/uuid"
)

func (h HTTPServer) PostUser(w http.ResponseWriter, r *http.Request) {
	//TODO implement me
	panic("implement me")
}

func (h HTTPServer) GetUserList(w http.ResponseWriter, r *http.Request, params GetUserListParams) {
	//TODO implement me
	panic("implement me")
}

func (h HTTPServer) UpdateUserDetails(w http.ResponseWriter, r *http.Request, id string) {
	//TODO implement me
	panic("implement me")
}

func (h HTTPServer) GetUser(w http.ResponseWriter, r *http.Request, id string) {
	corrID := uuid.NewString()
	user, err := h.app.Queries.GetUser.Handle(r.Context(), &query.GetUser{ID: id})
	if err != nil {
		httperr.InternalError(internalServerErrorLabel, getUser, corrID, err, w, r)
		return
	}
	resp := mapToResponse(user)
	render.Respond(w, r, resp)
}

func mapToResponse(user *domain.User) *GetUser {
	return &GetUser{
		Aszf:      user.ASZF,
		Email:     user.Email,
		FirstName: user.FirstName,
		Id:        uuid.MustParse(user.ID),
		LastName:  user.LastName,
		Mobile:    user.Mobile,
		UserName:  user.UserName,
	}
}
