package psql

import (
	"context"
	"it-test/domain"

	"github.com/uptrace/bun"
)

type User struct {
	//nolint:structcheck,gocritic,unused
	tableName struct{} `bun:"users"`
	ID        string   `bun:"id,notnull"`
	UserName  string   `bun:"user_name,notnull"`
	LastName  string   `bun:"last_name,notnull"`
	FirstName string   `bun:"first_name,notnull"`
	Password  string   `bun:"password,notnull"`
	Email     string   `bun:"email,notnull"`
	Mobile    string   `bun:"mobile,notnull"`
	ASZF      bool     `bun:"aszf,notnull"`
}

type UserPSQLRepository struct {
	db bun.IDB
}

func NewUserPSQLRepository(db *bun.DB) *UserPSQLRepository {
	return &UserPSQLRepository{db: db}
}

func (r *UserPSQLRepository) GetUser(
	ctx context.Context, id string) (*domain.User, error) {
	user := new(User)
	err := r.db.NewSelect().
		Where("id = ?", id).
		Model(user).
		Scan(ctx)
	if err != nil {
		return nil, err
	}
	return toDomain(user), nil
}

func toDomain(user *User) *domain.User {
	return &domain.User{
		ID:        user.ID,
		UserName:  user.UserName,
		LastName:  user.LastName,
		FirstName: user.FirstName,
		Password:  user.Password,
		Email:     user.Email,
		Mobile:    user.Mobile,
		ASZF:      user.ASZF,
	}
}
