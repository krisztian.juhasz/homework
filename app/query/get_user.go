package query

import (
	"context"
	"it-test/domain"
)

type GetUserRepository interface {
	GetUser(ctx context.Context, ID string) (*domain.User, error)
}

type GetUserHandler struct {
	repo GetUserRepository
}

type GetUser struct {
	ID string
}

func NewGetUserHandler(repo GetUserRepository) *GetUserHandler {
	return &GetUserHandler{repo: repo}
}

func (h *GetUserHandler) Handle(ctx context.Context, cmd *GetUser) (user *domain.User, err error) {
	return h.repo.GetUser(ctx, cmd.ID)
}
